# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the teleports.ubports package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: teleports.ubports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-14 20:46+0000\n"
"PO-Revision-Date: 2020-11-23 18:30+0000\n"
"Last-Translator: Jiri Grönroos <jiri.gronroos@iki.fi>\n"
"Language-Team: Finnish <https://translate.ubports.com/projects/ubports/"
"teleports/fi/>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#: ../app/qml/actions/BackAction.qml:10
msgid "Back"
msgstr "Takaisin"

#: ../app/qml/components/CountryPicker.qml:21
#: ../app/qml/pages/WaitPhoneNumberPage.qml:36
msgid "Choose a country"
msgstr "Valitse maa"

#: ../app/qml/components/CountryPicker.qml:38
msgid "Search country name..."
msgstr "Hae maan nimellä..."

#: ../app/qml/components/DeleteDialog.qml:9
msgid ""
"The message will be deleted for all users in the chat. Do you really want to "
"delete it?"
msgstr ""
"Viesti poistetaan kaikilta keskusteluun osallistuvilta. Haluatko todella "
"poistaa sen?"

#: ../app/qml/components/DeleteDialog.qml:10
msgid ""
"The message will be deleted only for you. Do you really want to delete it?"
msgstr "Viesti poistetaan vain sinulta. Haluatko todella poistaa sen?"

#: ../app/qml/components/DeleteDialog.qml:12
#: ../app/qml/delegates/MessageBubbleItem.qml:37
#: ../app/qml/pages/SettingsPage.qml:151 ../app/qml/pages/UserListPage.qml:121
#: ../app/qml/pages/UserListPage.qml:215
msgid "Delete"
msgstr "Poista"

#: ../app/qml/components/GroupPreviewDialog.qml:35
#: ../app/qml/pages/MessageListPage.qml:33
msgid "%1 member"
msgid_plural "%1 members"
msgstr[0] "%1 jäsen"
msgstr[1] "%1 jäsentä"

#: ../app/qml/components/GroupPreviewDialog.qml:37
msgid "%1 members, among them:"
msgstr ""

#: ../app/qml/components/InputInfoBox.qml:91
msgid "Edit message"
msgstr "Muokkaa viestiä"

#: ../app/qml/components/MessageStatusRow.qml:36
msgid "Edited"
msgstr "Muokattu"

#: ../app/qml/components/PopupDialog.qml:14
msgid "Okay"
msgstr "OK"

#: ../app/qml/components/PopupDialog.qml:15
#: ../app/qml/components/PopupWaitCancel.qml:13
#: ../app/qml/pages/ChatListPage.qml:32
msgid "Cancel"
msgstr "Peru"

#: ../app/qml/components/UserProfile.qml:74
msgid "Members: %1"
msgstr "Jäseniä: %1"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:6
#, fuzzy
#| msgid "Channel called << %1 >> created"
msgid "Channel called <b>%1</b> created"
msgstr "Kanava nimeltä << %1 >> luotu"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:7
#, fuzzy
#| msgid "%1 created a group called << %2 >>"
msgid "%1 created a group called <b>%2</b>"
msgstr "%1 loi ryhmän nimeltä << %2 >>"

#: ../app/qml/delegates/MessageBubbleItem.qml:49
msgid "Copy"
msgstr "Kopioi"

#: ../app/qml/delegates/MessageBubbleItem.qml:59
#: ../app/qml/pages/ChatInfoPage.qml:34
msgid "Edit"
msgstr "Muokkaa"

#: ../app/qml/delegates/MessageBubbleItem.qml:67
msgid "Reply"
msgstr "Vastaa"

#: ../app/qml/delegates/MessageBubbleItem.qml:73
msgid "Sticker Pack info"
msgstr "Tarrapaketin tietoja"

#: ../app/qml/delegates/MessageBubbleItem.qml:79
#: ../app/qml/pages/ChatListPage.qml:440
msgid "Forward"
msgstr "Edelleenlähetä"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:8
msgid "%1 joined the group"
msgstr "%1 liittyi ryhmään"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:9
msgid "%1 added %2"
msgstr "%1 lisäsi henkilön %2"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:10
#, fuzzy
#| msgid "%1 joined the group"
msgid "Unknown joined group"
msgstr "%1 liittyi ryhmään"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:32
msgid "%1 user(s)"
msgid_plural ""
msgstr[0] "%1 käyttäjä"
msgstr[1] "%1 käyttäjää"

#: ../app/qml/delegates/MessageChatChangePhoto.qml:32
#, fuzzy
#| msgid "message TTL has been changed"
msgid "Channel photo has been changed:"
msgstr "viestin TTL on muutettu"

#: ../app/qml/delegates/MessageChatChangePhoto.qml:33
#, fuzzy
#| msgid "changed the chat photo"
msgid "%1 changed the chat photo:"
msgstr "muutti keskustelun kuvaa"

#: ../app/qml/delegates/MessageChatChangeTitle.qml:6
msgid "Channel title has been changed to <b>%1</b>"
msgstr ""

#: ../app/qml/delegates/MessageChatChangeTitle.qml:7
#, fuzzy
#| msgid "changed the chat title"
msgid "%1 changed the chat title to <b>%2</b>"
msgstr "muutti keskustelun otsikkoa"

#: ../app/qml/delegates/MessageChatDeleteMember.qml:6
msgid "%1 left the group"
msgstr "%1 poistui ryhmästä"

#. TRANSLATORS: Notification message saying: person A removed person B (from a group)
#: ../app/qml/delegates/MessageChatDeleteMember.qml:7
#: ../push/pushhelper.cpp:238
msgid "%1 removed %2"
msgstr "%1 poisti henkilön %2"

#: ../app/qml/delegates/MessageChatDeletePhoto.qml:6
msgid "Channel photo has been removed"
msgstr ""

#: ../app/qml/delegates/MessageChatDeletePhoto.qml:7
#, fuzzy
#| msgid "deleted the chat photo"
msgid "%1 deleted the chat photo"
msgstr "poisti keskustelun kuvan"

#: ../app/qml/delegates/MessageChatSetTTL.qml:4
msgid "Message time-to-live has been set to <b>%1</b> seconds"
msgstr ""

#: ../app/qml/delegates/MessageChatUpgradeFrom.qml:4
#: ../app/qml/delegates/MessageChatUpgradeTo.qml:4
msgid "Group has been upgraded to Supergroup"
msgstr "Ryhmä on päivitetty superryhmäksi"

#: ../app/qml/delegates/MessageContactRegistered.qml:5
msgid "%1 has joined Telegram!"
msgstr "%1 liittyi Telegramiin!"

#: ../app/qml/delegates/MessageContentBase.qml:37
msgid "Forwarded from %1"
msgstr "Edelleenlähetetty henkilöltä %1"

#. TRANSLATORS: This is the duration of a phone call in hours:minutes:seconds format
#: ../app/qml/delegates/MessageContentCall.qml:59
msgid "Duration: %1:%2:%3"
msgstr "Kesto: %1:%2:%3"

#: ../app/qml/delegates/MessageContentVoiceNote.qml:56
msgid "Voice note"
msgstr "Puheviesti"

#: ../app/qml/delegates/MessageDateItem.qml:4
msgid "Some date missing"
msgstr ""

#: ../app/qml/delegates/MessageJoinByLink.qml:5
msgid "%1 joined by invite link"
msgstr "%1 liittyi kutsulinkin kautta"

#: ../app/qml/delegates/MessagePinMessage.qml:6
#: ../app/qml/delegates/MessagePinMessage.qml:7 ../push/pushhelper.cpp:301
msgid "%1 pinned a message"
msgstr "%1 kiinnitti viestin"

#: ../app/qml/delegates/MessageScreenshotTaken.qml:4
msgid "A screenshot has been taken"
msgstr "Kuvakaappaus otettu"

#: ../app/qml/delegates/MessageUnavailable.qml:11
msgid "Message Unavailable..."
msgstr ""

#: ../app/qml/delegates/MessageUnreadLabelItem.qml:4
msgid "Missing label..."
msgstr "Nimiöinti puuttuu..."

#: ../app/qml/delegates/MessageUnsupported.qml:4
#: ../libs/qtdlib/messages/content/qtdmessageunsupported.cpp:12
msgid "Unsupported message"
msgstr "Ei tuettu viesti"

#: ../app/qml/delegates/NotImplementedYet.qml:12
msgid "Unknown message type, see logfile for details..."
msgstr "Tunnistamaton viestityyppi, katso tarkemmat tiedot lokitiedostosta..."

#: ../app/qml/middleware/ChatMiddleware.qml:27
msgid "Are you sure you want to clear the history?"
msgstr "Oletko varma että haluat tyhjentää historian?"

#: ../app/qml/middleware/ChatMiddleware.qml:28
#: ../app/qml/pages/ChatListPage.qml:210
msgid "Clear history"
msgstr "Tyhjennä historia"

#: ../app/qml/middleware/ChatMiddleware.qml:37
msgid "Are you sure you want to leave this chat?"
msgstr "Oletko varma että haluat poistua tästä keskustelusta?"

#: ../app/qml/middleware/ChatMiddleware.qml:38
msgid "Leave"
msgstr "Poistu"

#: ../app/qml/middleware/ChatMiddleware.qml:47
#, fuzzy
#| msgid "joined the group"
msgid "Join group"
msgstr "liittyi ryhmään"

#: ../app/qml/middleware/ErrorsMiddleware.qml:19
msgid "Close"
msgstr "Sulje"

#: ../app/qml/pages/AboutPage.qml:13 ../app/qml/pages/ChatListPage.qml:156
msgid "About"
msgstr "Yleistä"

#. TRANSLATORS: Application name.
#: ../app/qml/pages/AboutPage.qml:65 ../push/pushhelper.cpp:114
#: teleports.desktop.in.h:1
msgid "TELEports"
msgstr "TELEportit"

#: ../app/qml/pages/AboutPage.qml:71
msgid "Version %1"
msgstr "Versio %1"

#: ../app/qml/pages/AboutPage.qml:73
msgid " (git# %1)"
msgstr " (git# %1)"

#: ../app/qml/pages/AboutPage.qml:92
msgid "Get the source"
msgstr "Lähdekoodi"

#: ../app/qml/pages/AboutPage.qml:93
msgid "Report issues"
msgstr "Ilmoita ongelmasta"

#: ../app/qml/pages/AboutPage.qml:94
msgid "Help translate"
msgstr "Auta käännöksessä"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Group Details"
msgstr "Ryhmän tiedot"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Profile"
msgstr "Profiili"

#: ../app/qml/pages/ChatInfoPage.qml:40
msgid "Send message"
msgstr "Lähetä viesti"

#: ../app/qml/pages/ChatInfoPage.qml:60
msgid "Edit user data and press Save"
msgstr "Muokkaa käyttäjän tietoja ja paina Tallenna"

#: ../app/qml/pages/ChatInfoPage.qml:62 ../app/qml/pages/PreviewPage.qml:49
msgid "Save"
msgstr "Tallenna"

#: ../app/qml/pages/ChatInfoPage.qml:73 ../app/qml/pages/UserListPage.qml:67
msgid "Phone no"
msgstr "Puhelinnro"

#: ../app/qml/pages/ChatInfoPage.qml:82 ../app/qml/pages/UserListPage.qml:75
msgid "First name"
msgstr "Etunimi"

#: ../app/qml/pages/ChatInfoPage.qml:91 ../app/qml/pages/UserListPage.qml:83
msgid "Last name"
msgstr "Sukunimi"

#: ../app/qml/pages/ChatInfoPage.qml:130
msgid "Notifications"
msgstr "Ilmoitukset"

#: ../app/qml/pages/ChatInfoPage.qml:159
msgid "%1 group in common"
msgid_plural "%1 groups in common"
msgstr[0] "%1 yhteinen ryhmä"
msgstr[1] "%1 yhteistä ryhmää"

#: ../app/qml/pages/ChatInfoPage.qml:184
#: ../app/qml/pages/SecretChatKeyHashPage.qml:15
msgid "Encryption Key"
msgstr "Salausavain"

#: ../app/qml/pages/ChatListPage.qml:21
msgid "Select destination or cancel..."
msgstr "Valitse määränpää tai keskeytä..."

#: ../app/qml/pages/ChatListPage.qml:40 ../app/qml/pages/ChatListPage.qml:137
#: ../app/qml/pages/SettingsPage.qml:22
msgid "Settings"
msgstr "Asetukset"

#: ../app/qml/pages/ChatListPage.qml:63
msgid "Search"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:113
msgid "All"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:113
#, fuzzy
#| msgid "Archived chats"
msgid "Archived"
msgstr "Tallennetut keskustelut"

#: ../app/qml/pages/ChatListPage.qml:113
msgid "Personal"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:113
msgid "Unread"
msgstr ""

#: ../app/qml/pages/ChatListPage.qml:127 ../libs/qtdlib/chat/qtdchat.cpp:87
msgid "Saved Messages"
msgstr "Tallennetut viestit"

#: ../app/qml/pages/ChatListPage.qml:132 ../app/qml/pages/UserListPage.qml:19
msgid "Contacts"
msgstr "Yhteystiedot"

#: ../app/qml/pages/ChatListPage.qml:143
msgid "Night mode"
msgstr "Yötila"

#: ../app/qml/pages/ChatListPage.qml:205
msgid "Leave chat"
msgstr "Poistu keskustelusta"

#: ../app/qml/pages/ChatListPage.qml:221 ../app/qml/pages/UserListPage.qml:131
msgid "Info"
msgstr "Info"

#: ../app/qml/pages/ChatListPage.qml:438
msgid "Do you want to forward the selected messages to %1?"
msgstr "Haluatko edelleenlähettää valitut viestit henkilölle %1?"

#: ../app/qml/pages/ChatListPage.qml:453 ../app/qml/pages/ChatListPage.qml:478
msgid "Enter optional message..."
msgstr "Syötä vaihtoehtoinen viesti..."

#: ../app/qml/pages/ChatListPage.qml:461
msgid "Do you want to send the imported files to %1?"
msgstr "Haluatko lähettää tuodut viestit henkilölle %1?"

#: ../app/qml/pages/ChatListPage.qml:462
#, fuzzy
#| msgid "Do you want to send the imported files to %1?"
msgid "Do you want to send the imported text to %1?"
msgstr "Haluatko lähettää tuodut viestit henkilölle %1?"

#: ../app/qml/pages/ChatListPage.qml:464
#: ../app/qml/pages/MessageListPage.qml:854
msgid "Send"
msgstr "Lähetä"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:28
msgid "Connecting"
msgstr "Yhdistetään"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:26
msgid "Offline"
msgstr "Ei paikalla"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:30
#: ../libs/qtdlib/user/qtduserstatus.cpp:78
msgid "Online"
msgstr "Paikalla"

#: ../app/qml/pages/ConnectivityPage.qml:27
msgid "Connecting To Proxy"
msgstr "Yhdistetään välityspalvelimeen"

#: ../app/qml/pages/ConnectivityPage.qml:29
msgid "Updating"
msgstr "Päivitetään"

#: ../app/qml/pages/ConnectivityPage.qml:35
msgid "Connectivity"
msgstr "Yhteyden tila"

#: ../app/qml/pages/ConnectivityPage.qml:73
msgid "Telegram connectivity status:"
msgstr "Telegramin yhteyden tila:"

#: ../app/qml/pages/ConnectivityPage.qml:80
msgid "Ubuntu Touch connectivity status:"
msgstr "Ubuntu Touchin yhteyden tila:"

#: ../app/qml/pages/ConnectivityPage.qml:87
msgid "Ubuntu Touch bandwith limited"
msgstr "Ubuntu Touchin kaistanleveys on rajoitettu"

#: ../app/qml/pages/ConnectivityPage.qml:87
msgid "Ubuntu Touch bandwith not limited"
msgstr "Ubuntu Touchin kaistanleveyttä ei ole rajoitettu"

#: ../app/qml/pages/LogoutPage.qml:14
msgid "Good bye!"
msgstr ""

#: ../app/qml/pages/LogoutPage.qml:27
#, fuzzy
#| msgid "Connecting"
msgid "Disconnecting..."
msgstr "Yhdistetään"

#: ../app/qml/pages/LogoutPage.qml:37
msgid ""
"The app will close automatically when the logout process ends.\n"
"Please, don't close it manually!"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:35
msgid ", %1 online"
msgid_plural ", %1 online"
msgstr[0] ", %1 paikalla"
msgstr[1] ", %1 paikalla"

#: ../app/qml/pages/MessageListPage.qml:131
msgid "Telegram"
msgstr "Telegram-viestitin"

#: ../app/qml/pages/MessageListPage.qml:337
msgid "You are not allowed to post in this channel"
msgstr "Sinulla ei ole lupaa lähettää viestejä tälle kanavalle"

#: ../app/qml/pages/MessageListPage.qml:341
msgid "Waiting for other party to accept the secret chat..."
msgstr "Odotetaan, että toinen osapuoli hyväksyy salaisen keskustelun..."

#: ../app/qml/pages/MessageListPage.qml:343
msgid "Secret chat has been closed"
msgstr "Salainen keskustelu on suljettu"

#: ../app/qml/pages/MessageListPage.qml:351
#, fuzzy
#| msgid "left the group"
msgid "You left this group"
msgstr "poistui ryhmästä"

#: ../app/qml/pages/MessageListPage.qml:354
#, fuzzy
#| msgid "You have a new message"
msgid "You have been banned"
msgstr "Sinulle on uusi viesti"

#: ../app/qml/pages/MessageListPage.qml:357
#, fuzzy
#| msgid "You are not allowed to post in this channel"
msgid "You are not allowed to post in this group"
msgstr "Sinulla ei ole lupaa lähettää viestejä tälle kanavalle"

#: ../app/qml/pages/MessageListPage.qml:361
msgid "You can't write here. Reason unkown"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:384
msgid "Join"
msgstr ""

#: ../app/qml/pages/MessageListPage.qml:524
msgid "Type a message..."
msgstr "Kirjoita viesti..."

#: ../app/qml/pages/MessageListPage.qml:742
msgid "<<< Swipe to cancel"
msgstr "<<< Pyyhkäise peruaksesi"

#: ../app/qml/pages/MessageListPage.qml:852
msgid "Do you want to share your location with %1?"
msgstr "Haluatko jakaa sijaintisi henkilön %1 kanssa?"

#: ../app/qml/pages/MessageListPage.qml:865
msgid "Requesting location from OS..."
msgstr "Pyydetään sijaintia käyttöjärjestelmältä..."

#: ../app/qml/pages/PickerPage.qml:16
msgid "Content Picker"
msgstr "Sisällönvalitsin"

#: ../app/qml/pages/PreviewPage.qml:35
msgid "File: "
msgstr "Tiedosto: "

#: ../app/qml/pages/SecretChatKeyHashPage.qml:66
msgid ""
"Check the image or the text. If they match with the ones on <b>%1</b>'s "
"device, end-to-end cryptography is granted."
msgstr ""
"Tarkista kuva tai teksti. Mikäli täsmäävät <b>%1</b>:n laitteessa olevien "
"kanssa, päästäpäähän salaustekniikka myönnetään."

#: ../app/qml/pages/SettingsPage.qml:69 ../app/qml/pages/SettingsPage.qml:141
msgid "Logout"
msgstr "Kirjaudu ulos"

#: ../app/qml/pages/SettingsPage.qml:85
msgid "Delete account"
msgstr "Poista käyttäjätili"

#: ../app/qml/pages/SettingsPage.qml:101
msgid "Connectivity status"
msgstr "Yhteyden tila"

#: ../app/qml/pages/SettingsPage.qml:118
msgid "Toggle message status indicators"
msgstr ""

#: ../app/qml/pages/SettingsPage.qml:139
msgid ""
"Warning: Logging out will delete all local data from this device, including "
"secret chats. Are you still sure you want to log out?"
msgstr ""
"Varoitus: Uloskirjautuminen poistaa kaiken paikallisen datan tästä "
"laitteesta, salaiset keskustelut mukaanlukien. Oletko varma että haluat "
"kirjautua ulos?"

#: ../app/qml/pages/SettingsPage.qml:149
#, fuzzy
#| msgid ""
#| "Warning: Deleting the account will delete all the data you ever received "
#| "or send using telegram except for data you have explicitly saved outside "
#| "the telegram cloud. Are you really really sure you want to delete your "
#| "telegram account?"
msgid ""
"Warning: Deleting the account will delete all the data you ever received or "
"sent using telegram except for data you have explicitly saved outside the "
"telegram cloud. Are you really really sure you want to delete your telegram "
"account?"
msgstr ""
"Varoitus: Käyttäjätilin poistaminen poistaa kaiken datan, jonka olet "
"vastaanottanut tai lähettänyt telegramia käyttäen, paitsi data, jonka olet "
"tallettanut telegram pilven ulkopuolelle. Oletko todellakin varma, että "
"haluat poistaa telegram käyttäjätilisi?"

#: ../app/qml/pages/UserListPage.qml:38
msgid "Add Contact"
msgstr "Lisää yhteystieto"

#: ../app/qml/pages/UserListPage.qml:43
#, fuzzy
#| msgid "Contacts"
msgid "Import Contact"
msgstr "Yhteystiedot"

#: ../app/qml/pages/UserListPage.qml:55
msgid "The contact will be added. First and last name are optional"
msgstr "Lisätään yhteystieto. Etu- ja sukunimi ovat valinnaisia"

#: ../app/qml/pages/UserListPage.qml:57
msgid "Add"
msgstr "Lisää"

#: ../app/qml/pages/UserListPage.qml:136
msgid "Secret Chat"
msgstr "Salainen keskustelu"

#: ../app/qml/pages/UserListPage.qml:213
msgid "The contact will be deleted. Are you sure?"
msgstr "Yhteystieto poistetaan. Oletko varma?"

#: ../app/qml/pages/WaitCodePage.qml:17
msgid "Enter Code"
msgstr "Syötä koodi"

#: ../app/qml/pages/WaitCodePage.qml:39
msgid "Code"
msgstr "Koodi"

#: ../app/qml/pages/WaitCodePage.qml:54
#, fuzzy
#| msgid "We've send a code via telegram to your device. Please enter it here."
msgid ""
"A code was sent via Telegram to your other devices. Please enter it here."
msgstr ""
"Lähetimme koodin Telegramin kautta laitteeseesi. Ole hyvä ja syötä se tänne."

#: ../app/qml/pages/WaitPasswordPage.qml:18
msgid "Enter Password"
msgstr "Syötä salasana"

#: ../app/qml/pages/WaitPasswordPage.qml:39
msgid "Password"
msgstr "Salasana"

#: ../app/qml/pages/WaitPasswordPage.qml:57
#, fuzzy
#| msgid "Password"
msgid "Password hint: %1"
msgstr "Salasana"

#: ../app/qml/pages/WaitPasswordPage.qml:62
#: ../app/qml/pages/WaitPhoneNumberPage.qml:92
#: ../app/qml/pages/WaitRegistrationPage.qml:57
msgid "Next..."
msgstr "Seuraava..."

#: ../app/qml/pages/WaitPhoneNumberPage.qml:17
msgid "Enter Phone Number"
msgstr "Syötä puhelinnumero"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:54
#: ../app/qml/pages/WaitPhoneNumberPage.qml:64
msgid "Phone number"
msgstr "Puhelinnumero"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:88
msgid "Please confirm your country code and enter your phone number."
msgstr "Ole hyvä ja varmista maakoodi ja syötä puhelinnumerosi."

#: ../app/qml/pages/WaitRegistrationPage.qml:17
msgid "Enter your Name"
msgstr "Kirjoita nimesi"

#: ../app/qml/pages/WaitRegistrationPage.qml:38
msgid "First Name"
msgstr "Etunimi"

#: ../app/qml/pages/WaitRegistrationPage.qml:44
msgid "Last Name"
msgstr "Sukunimi"

#: ../app/qml/stores/AuthStateStore.qml:60
msgid "Invalid phone number!"
msgstr "Epäkelpo puhelinnumero!"

#: ../app/qml/stores/AuthStateStore.qml:67
msgid "Invalid code!"
msgstr "Epäkelpo koodi!"

#: ../app/qml/stores/AuthStateStore.qml:74
msgid "Invalid password!"
msgstr "Epäkelpo salasana!"

#: ../app/qml/stores/AuthStateStore.qml:94
msgid "Auth code not expected right now"
msgstr "Varmistuskoodia ei odotettu juuri nyt"

#: ../app/qml/stores/AuthStateStore.qml:100
msgid "Oops! Internal error."
msgstr "Ups! Sisäinen virhe."

#: ../app/qml/stores/AuthStateStore.qml:119
#, fuzzy
#| msgid "Auth code not expected right now"
msgid "Registration not expected right now"
msgstr "Varmistuskoodia ei odotettu juuri nyt"

#: ../app/qml/stores/ChatStateStore.qml:42
#: ../app/qml/stores/ChatStateStore.qml:48
msgid "Error"
msgstr "Virhe"

#: ../app/qml/stores/ChatStateStore.qml:42
msgid "No valid location received after 180 seconds!"
msgstr "Validia sijaintia ei vastaanotettu 180 sekunnissa!"

#: ../app/qml/stores/ChatStateStore.qml:48
msgid "Username <b>@%1</b> not found"
msgstr ""

#: ../app/qml/stores/NotificationsStateStore.qml:12
msgid "Push Registration Failed"
msgstr "Push- rekisteröinti epäonnistui"

#: ../app/qml/stores/NotificationsStateStore.qml:23
msgid "No Ubuntu One"
msgstr "Ei Ubuntu Onea"

#: ../app/qml/stores/NotificationsStateStore.qml:24
msgid "Please connect to Ubuntu One to receive push notifications."
msgstr ""
"Ole hyvä ja kirjaudu Ubuntu One- palveluun saadaksesi push- ilmoituksia."

#: ../libs/qtdlib/chat/qtdchat.cpp:326
msgid "Draft:"
msgstr "Luonnos:"

#: ../libs/qtdlib/chat/qtdchat.cpp:622
msgid "is choosing contact..."
msgstr "valitsee kontaktia..."

#: ../libs/qtdlib/chat/qtdchat.cpp:623
msgid "are choosing contact..."
msgstr "ovat valitsemassa kontaktia..."

#: ../libs/qtdlib/chat/qtdchat.cpp:626
msgid "is choosing location..."
msgstr "valitsee sijaintia..."

#: ../libs/qtdlib/chat/qtdchat.cpp:627
msgid "are choosing location..."
msgstr "ovat valitsemassa sijaintia..."

#: ../libs/qtdlib/chat/qtdchat.cpp:632
msgid "is recording..."
msgstr "on äänittämässä..."

#: ../libs/qtdlib/chat/qtdchat.cpp:633
msgid "are recording..."
msgstr "ovat äänittämässä..."

#: ../libs/qtdlib/chat/qtdchat.cpp:636
msgid "is typing..."
msgstr "on kirjoittamassa..."

#: ../libs/qtdlib/chat/qtdchat.cpp:637
msgid "are typing..."
msgstr "ovat kirjoittamassa..."

#: ../libs/qtdlib/chat/qtdchat.cpp:640
msgid "is doing something..."
msgstr "tekee jotakin..."

#: ../libs/qtdlib/chat/qtdchat.cpp:641
msgid "are doing something..."
msgstr "tekevät jotakin..."

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF"
msgstr "GIF-muoto"

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF,"
msgstr "GIF,"

#: ../libs/qtdlib/messages/content/qtdmessagebasicgroupchatcreate.cpp:25
#: ../libs/qtdlib/messages/content/qtdmessagesupergroupchatcreate.cpp:23
msgid "created this group"
msgstr "loi tämän ryhmän"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:68
msgid "Call Declined"
msgstr "Puhelu hylätty"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:71
msgid "Call Disconnected"
msgstr "Puhelu katkaistu"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:74
msgid "Call Ended"
msgstr "Puhelu päättyi"

#. TRANSLATORS: This is a duration in hours:minutes:seconds format - only arrange the order, do not translate!
#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:78
msgid "%1:%2:%3"
msgstr "%1:%2:%3"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:79
msgid "Outgoing Call (%1)"
msgstr "Lähtevä puhelu (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:80
msgid "Incoming Call (%1)"
msgstr "Saapuva puhelu"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Cancelled Call"
msgstr "Puhelu peruttu"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Missed Call"
msgstr "Vastaamaton puhelu"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:86
msgid "Call"
msgstr "Soita"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "added one or more members"
msgstr "lisäsi yhden tai useamman käyttäjän"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "joined the group"
msgstr "liittyi ryhmään"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangephoto.cpp:20
msgid "changed the chat photo"
msgstr "muutti keskustelun kuvaa"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangetitle.cpp:19
msgid "changed the chat title"
msgstr "muutti keskustelun otsikkoa"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "left the group"
msgstr "poistui ryhmästä"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "removed a member"
msgstr "poisti jäsenen"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletephoto.cpp:8
msgid "deleted the chat photo"
msgstr "poisti keskustelun kuvan"

#: ../libs/qtdlib/messages/content/qtdmessagechatjoinbylink.cpp:8
msgid "joined the group via the public link"
msgstr "liittyi ryhmään julkisen linkin kautta"

#: ../libs/qtdlib/messages/content/qtdmessagechatsetttl.cpp:19
msgid "message TTL has been changed"
msgstr "viestin TTL on muutettu"

#: ../libs/qtdlib/messages/content/qtdmessagechatupgradefrom.cpp:29
#: ../libs/qtdlib/messages/content/qtdmessagechatupgradeto.cpp:29
msgid "upgraded to supergroup"
msgstr "päivitettiin superryhmäksi"

#: ../libs/qtdlib/messages/content/qtdmessagecontact.cpp:18
#, fuzzy
#| msgid "Contacts"
msgid "Contact"
msgstr "Yhteystiedot"

#: ../libs/qtdlib/messages/content/qtdmessagecontactregistered.cpp:9
msgid "has joined Telegram!"
msgstr "liittyi Telegramiin!"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:19
msgid "Today"
msgstr "Tänään"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:21
msgid "Yesterday"
msgstr "Eilen"

#. TRANSLATORS: String in date separator label. For messages within a week: full weekday name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:24
msgid "dddd"
msgstr "dddd"

#. TRANSLATORS: String in date separator label. For messages of pas years: date number, month name and year
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:27
msgid "dd MMMM yyyy"
msgstr "dd. MMMMta yyyy"

#. TRANSLATORS: String in date separator label. For messages older that a week but within the current year: date number and month name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:30
msgid "dd MMMM"
msgstr "dd. MMMM"

#: ../libs/qtdlib/messages/content/qtdmessagelocation.cpp:19
msgid "Location"
msgstr "Sijainti"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo"
msgstr "Valokuva"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo,"
msgstr "Kuva,"

#: ../libs/qtdlib/messages/content/qtdmessagepinmessage.cpp:19
#, fuzzy
#| msgid "Unread Messages"
msgid "Pinned Message"
msgstr "Lukemattomat viestit"

#: ../libs/qtdlib/messages/content/qtdmessagesticker.cpp:20
msgid "Sticker"
msgstr "Tarra"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video"
msgstr "Video"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video,"
msgstr "Video,"

#: ../libs/qtdlib/messages/content/qtdmessagevideonote.cpp:30
msgid "Video message"
msgstr "Videoviesti"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message"
msgstr "Ääniviesti"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message,"
msgstr "Ääniviesti,"

#: ../libs/qtdlib/messages/forwardinfo/qtdmessageforwardinfo.cpp:61
#, fuzzy
#| msgid "%1 joined the group"
msgid "Unknown origin"
msgstr "%1 liittyi ryhmään"

#: ../libs/qtdlib/messages/qtdmessage.cpp:85
msgid "Me"
msgstr "Minä"

#: ../libs/qtdlib/messages/qtdmessagecontentfactory.cpp:133
msgid "Unimplemented:"
msgstr "Istuttamaton:"

#: ../libs/qtdlib/messages/qtdmessagelistmodel.cpp:250
msgid "Unread Messages"
msgstr "Lukemattomat viestit"

#: ../libs/qtdlib/user/qtduserstatus.cpp:28
msgid "Last seen one month ago"
msgstr "Nähty viimeksi kuukausi sitten"

#: ../libs/qtdlib/user/qtduserstatus.cpp:39
msgid "Last seen one week ago"
msgstr "Nähty viimeksi viikko sitten"

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "Last seen "
msgstr "Nähty viimeksi "

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "dd.MM.yy hh:mm"
msgstr "dd.MM.yy hh:mm"

#: ../libs/qtdlib/user/qtduserstatus.cpp:96
msgid "Seen recently"
msgstr "Nähty hiljattain"

#: ../push/pushhelper.cpp:124
msgid "sent you a message"
msgstr "lähetti sinulle viestin"

#: ../push/pushhelper.cpp:128
msgid "sent you a photo"
msgstr "lähetti sinulle kuvan"

#: ../push/pushhelper.cpp:132
msgid "sent you a sticker"
msgstr "lähetti sinulle tarran"

#: ../push/pushhelper.cpp:136
msgid "sent you a video"
msgstr "lähetti sinulle videon"

#: ../push/pushhelper.cpp:140
msgid "sent you a document"
msgstr "lähetti sinulle asiakirjan"

#: ../push/pushhelper.cpp:144
msgid "sent you an audio message"
msgstr "lähetti sinulle ääniviestin"

#: ../push/pushhelper.cpp:148
msgid "sent you a voice message"
msgstr "lähetti sinulle puheviestin"

#: ../push/pushhelper.cpp:152
msgid "shared a contact with you"
msgstr "jakoi yhteystiedon kanssasi"

#: ../push/pushhelper.cpp:156
msgid "sent you a map"
msgstr "lähetti sinulle kartan"

#: ../push/pushhelper.cpp:161
msgid "%1: %2"
msgstr "%1: %2"

#: ../push/pushhelper.cpp:166
msgid "%1 sent a message to the group"
msgstr "%1 lähetti viestin ryhmään"

#: ../push/pushhelper.cpp:171
msgid "%1 sent a photo to the group"
msgstr "%1 lähetti kuvan ryhmään"

#: ../push/pushhelper.cpp:176
msgid "%1 sent a sticker to the group"
msgstr "%1 lähetti tarran ryhmään"

#: ../push/pushhelper.cpp:181
msgid "%1 sent a video to the group"
msgstr "%1 lähetti videon ryhmään"

#: ../push/pushhelper.cpp:186
msgid "%1 sent a document to the group"
msgstr "%1 lähetti asiakirjan ryhmään"

#: ../push/pushhelper.cpp:191
msgid "%1 sent a voice message to the group"
msgstr "%1 lähetti puheviestin ryhmään"

#: ../push/pushhelper.cpp:196
msgid "%1 sent a GIF to the group"
msgstr "%1 lähetti GIF-kuvan ryhmään"

#: ../push/pushhelper.cpp:201
msgid "%1 sent a contact to the group"
msgstr "%1 lähetti yhteystiedon ryhmään"

#: ../push/pushhelper.cpp:206
msgid "%1 sent a map to the group"
msgstr "%1 lähetti kartan ryhmään"

#: ../push/pushhelper.cpp:211 ../push/pushhelper.cpp:232
msgid "%1 invited you to the group"
msgstr "%1 kutsui sinut ryhmään"

#: ../push/pushhelper.cpp:216
msgid "%1 changed group name"
msgstr "%1 muutti ryhmän nimeä"

#: ../push/pushhelper.cpp:221
msgid "%1 changed group photo"
msgstr "%1 muutti ryhmän kuvaa"

#. TRANSLATORS: Notification message saying: person A invited person B (to a group)
#: ../push/pushhelper.cpp:227
msgid "%1 invited %2"
msgstr "%1 kutsui henkilön %2"

#: ../push/pushhelper.cpp:243
msgid "%1 removed you from the group"
msgstr "%1 poisti sinut ryhmästä"

#: ../push/pushhelper.cpp:248
msgid "%1 has left the group"
msgstr "%1 poistui ryhmästä"

#: ../push/pushhelper.cpp:253
msgid "%1 has returned to the group"
msgstr "%1 palasi ryhmään"

#. TRANSLATORS: This format string tells location, like: @ McDonals, New York
#: ../push/pushhelper.cpp:258
msgid "@ %1"
msgstr "@ %1"

#. TRANSLATORS: This format string tells who has checked in (in a geographical location).
#: ../push/pushhelper.cpp:260
msgid "%1 has checked-in"
msgstr "%1 on saapunut paikalle"

#. TRANSLATORS: This format string tells who has just joined Telegram.
#: ../push/pushhelper.cpp:266
msgid "%1 joined Telegram!"
msgstr "%1 liittyi Telegramiin!"

#: ../push/pushhelper.cpp:271 ../push/pushhelper.cpp:277
msgid "New login from unrecognized device"
msgstr "Uusi kirjautuminen tuntemattomasta laitteesta"

#. TRANSLATORS: This format string indicates new login of: (device name) at (location).
#: ../push/pushhelper.cpp:276
msgid "%1 @ %2"
msgstr "%1 @ %2"

#: ../push/pushhelper.cpp:281
msgid "updated profile photo"
msgstr "päivitti profiilikuvan"

#: ../push/pushhelper.cpp:286 ../push/pushhelper.cpp:291
#: ../push/pushhelper.cpp:296
msgid "You have a new message"
msgstr "Sinulle on uusi viesti"

#~ msgid "Animated stickers not supported yet :("
#~ msgstr "Animoituja tarroja ei vielä tueta :("

#~ msgid "not available"
#~ msgstr "ei saatavilla"

#~ msgid "MMMM dd, yyyy"
#~ msgstr "dd. MM yyyy"

#~ msgid "MMMM dd"
#~ msgstr "dd. MMMMta"

#~ msgid "Incorrect auth code length."
#~ msgstr "Varmistuskoodi väärän pituinen."

#~ msgid "Phone call"
#~ msgstr "Puhelu"

#~ msgid "sent an audio message"
#~ msgstr "lähetti ääniviestin"

#~ msgid "sent a photo"
#~ msgstr "lähetti kuvan"

#~ msgid "sent a video"
#~ msgstr "lähetti videon"

#~ msgid "sent a video note"
#~ msgstr "lähetti videoviestin"

#~ msgid "sent a voice note"
#~ msgstr "lähetti puheviestin"

#~ msgid "joined by invite link"
#~ msgstr "liittyi kutsulinkin kautta"
