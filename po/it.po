# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the com.ubports.teleports package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: com.ubports.teleports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-07-14 20:46+0000\n"
"PO-Revision-Date: 2021-07-18 22:26+0000\n"
"Last-Translator: Mike <miguel2000@livecom.it>\n"
"Language-Team: Italian <https://translate.ubports.com/projects/ubports/"
"teleports/it/>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#: ../app/qml/actions/BackAction.qml:10
msgid "Back"
msgstr "Indietro"

#: ../app/qml/components/CountryPicker.qml:21
#: ../app/qml/pages/WaitPhoneNumberPage.qml:36
msgid "Choose a country"
msgstr "Scegli un Paese"

#: ../app/qml/components/CountryPicker.qml:38
msgid "Search country name..."
msgstr "Cerca nome del Paese..."

#: ../app/qml/components/DeleteDialog.qml:9
msgid ""
"The message will be deleted for all users in the chat. Do you really want to "
"delete it?"
msgstr ""
"Il messaggio sarà eliminato per tutti gli utenti nella chat. Sei sicuro di "
"volerlo eliminare?"

#: ../app/qml/components/DeleteDialog.qml:10
msgid ""
"The message will be deleted only for you. Do you really want to delete it?"
msgstr ""
"Il messaggio sarà eliminato solo per te. Sei sicuro di volerlo eliminare?"

#: ../app/qml/components/DeleteDialog.qml:12
#: ../app/qml/delegates/MessageBubbleItem.qml:37
#: ../app/qml/pages/SettingsPage.qml:151 ../app/qml/pages/UserListPage.qml:121
#: ../app/qml/pages/UserListPage.qml:215
msgid "Delete"
msgstr "Elimina"

#: ../app/qml/components/GroupPreviewDialog.qml:35
#: ../app/qml/pages/MessageListPage.qml:33
msgid "%1 member"
msgid_plural "%1 members"
msgstr[0] "%1 membro"
msgstr[1] "%1 membri"

#: ../app/qml/components/GroupPreviewDialog.qml:37
msgid "%1 members, among them:"
msgstr "%1 membri, tra cui:"

#: ../app/qml/components/InputInfoBox.qml:91
msgid "Edit message"
msgstr "Modifica messaggio"

#: ../app/qml/components/MessageStatusRow.qml:36
msgid "Edited"
msgstr "Modificato"

#: ../app/qml/components/PopupDialog.qml:14
msgid "Okay"
msgstr "Okay"

#: ../app/qml/components/PopupDialog.qml:15
#: ../app/qml/components/PopupWaitCancel.qml:13
#: ../app/qml/pages/ChatListPage.qml:32
msgid "Cancel"
msgstr "Annulla"

#: ../app/qml/components/UserProfile.qml:74
msgid "Members: %1"
msgstr "Membri: %1"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:6
msgid "Channel called <b>%1</b> created"
msgstr "Canale chiamato <b>%1</b> creato"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:7
msgid "%1 created a group called <b>%2</b>"
msgstr "%1 ha creato un gruppo chiamato <b>%2</b>"

#: ../app/qml/delegates/MessageBubbleItem.qml:49
msgid "Copy"
msgstr "Copia"

#: ../app/qml/delegates/MessageBubbleItem.qml:59
#: ../app/qml/pages/ChatInfoPage.qml:34
msgid "Edit"
msgstr "Modifica"

#: ../app/qml/delegates/MessageBubbleItem.qml:67
msgid "Reply"
msgstr "Rispondi"

#: ../app/qml/delegates/MessageBubbleItem.qml:73
msgid "Sticker Pack info"
msgstr "Info pacchetto di sticker"

#: ../app/qml/delegates/MessageBubbleItem.qml:79
#: ../app/qml/pages/ChatListPage.qml:440
msgid "Forward"
msgstr "Inoltra"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:8
msgid "%1 joined the group"
msgstr "%1 si é unito al gruppo"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:9
msgid "%1 added %2"
msgstr "%1 ha aggiunto %2"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:10
msgid "Unknown joined group"
msgstr "Sconosciuto si é unito al gruppo"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:32
msgid "%1 user(s)"
msgid_plural ""
msgstr[0] "%1 Utente"
msgstr[1] "%1 Utenti"

#: ../app/qml/delegates/MessageChatChangePhoto.qml:32
msgid "Channel photo has been changed:"
msgstr "La foto del canale è cambiata:"

#: ../app/qml/delegates/MessageChatChangePhoto.qml:33
msgid "%1 changed the chat photo:"
msgstr "%1 ha cambiato la foto della chat:"

#: ../app/qml/delegates/MessageChatChangeTitle.qml:6
msgid "Channel title has been changed to <b>%1</b>"
msgstr "Il titolo del canale è cambiato in <b>%1</b>"

#: ../app/qml/delegates/MessageChatChangeTitle.qml:7
msgid "%1 changed the chat title to <b>%2</b>"
msgstr "%1 ha cambiato il titolo della chat in <b>%2</b>"

#: ../app/qml/delegates/MessageChatDeleteMember.qml:6
msgid "%1 left the group"
msgstr "%1 ha lasciato il gruppo"

#. TRANSLATORS: Notification message saying: person A removed person B (from a group)
#: ../app/qml/delegates/MessageChatDeleteMember.qml:7
#: ../push/pushhelper.cpp:238
msgid "%1 removed %2"
msgstr "%1 ha rimosso %2"

#: ../app/qml/delegates/MessageChatDeletePhoto.qml:6
msgid "Channel photo has been removed"
msgstr "La foto del canale è stata rimossa"

#: ../app/qml/delegates/MessageChatDeletePhoto.qml:7
msgid "%1 deleted the chat photo"
msgstr "%1 ha eliminato la foto della chat"

#: ../app/qml/delegates/MessageChatSetTTL.qml:4
msgid "Message time-to-live has been set to <b>%1</b> seconds"
msgstr "La durata del messaggio è stata impostata a <b>%1</b> secondi"

#: ../app/qml/delegates/MessageChatUpgradeFrom.qml:4
#: ../app/qml/delegates/MessageChatUpgradeTo.qml:4
msgid "Group has been upgraded to Supergroup"
msgstr "Il gruppo è stato potenziato a Supergruppo"

#: ../app/qml/delegates/MessageContactRegistered.qml:5
msgid "%1 has joined Telegram!"
msgstr "%1 si è unito a Telegram!"

#: ../app/qml/delegates/MessageContentBase.qml:37
msgid "Forwarded from %1"
msgstr "Inoltrato da %1"

#. TRANSLATORS: This is the duration of a phone call in hours:minutes:seconds format
#: ../app/qml/delegates/MessageContentCall.qml:59
msgid "Duration: %1:%2:%3"
msgstr "Durata: %1:%2:%3"

#: ../app/qml/delegates/MessageContentVoiceNote.qml:56
msgid "Voice note"
msgstr "Nota vocale"

#: ../app/qml/delegates/MessageDateItem.qml:4
msgid "Some date missing"
msgstr "Alcune date mancanti"

#: ../app/qml/delegates/MessageJoinByLink.qml:5
msgid "%1 joined by invite link"
msgstr "%1 è entrato tramite link invito"

#: ../app/qml/delegates/MessagePinMessage.qml:6
#: ../app/qml/delegates/MessagePinMessage.qml:7 ../push/pushhelper.cpp:301
msgid "%1 pinned a message"
msgstr "%1 ha fissato un messaggio"

#: ../app/qml/delegates/MessageScreenshotTaken.qml:4
msgid "A screenshot has been taken"
msgstr "Screenshot acquisito"

#: ../app/qml/delegates/MessageUnavailable.qml:11
msgid "Message Unavailable..."
msgstr "Messaggio non disponibile..."

#: ../app/qml/delegates/MessageUnreadLabelItem.qml:4
msgid "Missing label..."
msgstr "Manca etichetta..."

#: ../app/qml/delegates/MessageUnsupported.qml:4
#: ../libs/qtdlib/messages/content/qtdmessageunsupported.cpp:12
msgid "Unsupported message"
msgstr "Messaggio non supportato"

#: ../app/qml/delegates/NotImplementedYet.qml:12
msgid "Unknown message type, see logfile for details..."
msgstr "Tipo di messaggio sconosciuto, guarda il file di log per dettagli..."

#: ../app/qml/middleware/ChatMiddleware.qml:27
msgid "Are you sure you want to clear the history?"
msgstr "Sei sicuro?"

#: ../app/qml/middleware/ChatMiddleware.qml:28
#: ../app/qml/pages/ChatListPage.qml:210
msgid "Clear history"
msgstr "Cancella cronologia"

#: ../app/qml/middleware/ChatMiddleware.qml:37
msgid "Are you sure you want to leave this chat?"
msgstr "Sei sicuro di voler lasciare questa chat?"

#: ../app/qml/middleware/ChatMiddleware.qml:38
msgid "Leave"
msgstr "Lascia"

#: ../app/qml/middleware/ChatMiddleware.qml:47
msgid "Join group"
msgstr "Entra nel gruppo"

#: ../app/qml/middleware/ErrorsMiddleware.qml:19
msgid "Close"
msgstr "Chiudi"

#: ../app/qml/pages/AboutPage.qml:13 ../app/qml/pages/ChatListPage.qml:156
msgid "About"
msgstr "Informazioni"

#. TRANSLATORS: Application name.
#: ../app/qml/pages/AboutPage.qml:65 ../push/pushhelper.cpp:114
#: teleports.desktop.in.h:1
msgid "TELEports"
msgstr "TELEports"

#: ../app/qml/pages/AboutPage.qml:71
msgid "Version %1"
msgstr "Versione %1"

#: ../app/qml/pages/AboutPage.qml:73
msgid " (git# %1)"
msgstr " (git# %1)"

#: ../app/qml/pages/AboutPage.qml:92
msgid "Get the source"
msgstr "Ottieni i sorgenti"

#: ../app/qml/pages/AboutPage.qml:93
msgid "Report issues"
msgstr "Segnala errori"

#: ../app/qml/pages/AboutPage.qml:94
msgid "Help translate"
msgstr "Aiuta a tradurre"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Group Details"
msgstr "Dettagli Gruppo"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Profile"
msgstr "Profilo"

#: ../app/qml/pages/ChatInfoPage.qml:40
msgid "Send message"
msgstr "Invia messaggio"

#: ../app/qml/pages/ChatInfoPage.qml:60
msgid "Edit user data and press Save"
msgstr "Modifica i dati dell'utente e premi Salva"

#: ../app/qml/pages/ChatInfoPage.qml:62 ../app/qml/pages/PreviewPage.qml:49
msgid "Save"
msgstr "Salva"

#: ../app/qml/pages/ChatInfoPage.qml:73 ../app/qml/pages/UserListPage.qml:67
msgid "Phone no"
msgstr "Numero di telefono"

#: ../app/qml/pages/ChatInfoPage.qml:82 ../app/qml/pages/UserListPage.qml:75
msgid "First name"
msgstr "Nome"

#: ../app/qml/pages/ChatInfoPage.qml:91 ../app/qml/pages/UserListPage.qml:83
msgid "Last name"
msgstr "Cognome"

#: ../app/qml/pages/ChatInfoPage.qml:130
msgid "Notifications"
msgstr "Notifiche"

#: ../app/qml/pages/ChatInfoPage.qml:159
msgid "%1 group in common"
msgid_plural "%1 groups in common"
msgstr[0] "%1 gruppo in comune"
msgstr[1] "%1 gruppi in comune"

#: ../app/qml/pages/ChatInfoPage.qml:184
#: ../app/qml/pages/SecretChatKeyHashPage.qml:15
msgid "Encryption Key"
msgstr "Chiave di crittografia"

#: ../app/qml/pages/ChatListPage.qml:21
msgid "Select destination or cancel..."
msgstr "Seleziona destinazione o annulla..."

#: ../app/qml/pages/ChatListPage.qml:40 ../app/qml/pages/ChatListPage.qml:137
#: ../app/qml/pages/SettingsPage.qml:22
msgid "Settings"
msgstr "Impostazioni"

#: ../app/qml/pages/ChatListPage.qml:63
msgid "Search"
msgstr "Cerca"

#: ../app/qml/pages/ChatListPage.qml:113
msgid "All"
msgstr "Tutte"

#: ../app/qml/pages/ChatListPage.qml:113
msgid "Archived"
msgstr "Archiviate"

#: ../app/qml/pages/ChatListPage.qml:113
msgid "Personal"
msgstr "Personali"

#: ../app/qml/pages/ChatListPage.qml:113
msgid "Unread"
msgstr "Non lette"

#: ../app/qml/pages/ChatListPage.qml:127 ../libs/qtdlib/chat/qtdchat.cpp:87
msgid "Saved Messages"
msgstr "Messaggi Salvati"

#: ../app/qml/pages/ChatListPage.qml:132 ../app/qml/pages/UserListPage.qml:19
msgid "Contacts"
msgstr "Contatti"

#: ../app/qml/pages/ChatListPage.qml:143
msgid "Night mode"
msgstr "Modalità notte"

#: ../app/qml/pages/ChatListPage.qml:205
msgid "Leave chat"
msgstr "Lascia chat"

#: ../app/qml/pages/ChatListPage.qml:221 ../app/qml/pages/UserListPage.qml:131
msgid "Info"
msgstr "Info"

#: ../app/qml/pages/ChatListPage.qml:438
msgid "Do you want to forward the selected messages to %1?"
msgstr "Vuoi inoltrare il messaggio selezionato a %1?"

#: ../app/qml/pages/ChatListPage.qml:453 ../app/qml/pages/ChatListPage.qml:478
msgid "Enter optional message..."
msgstr "Scrivi un messaggio opzionale..."

#: ../app/qml/pages/ChatListPage.qml:461
msgid "Do you want to send the imported files to %1?"
msgstr "Vuoi inviare i file importati a %1?"

#: ../app/qml/pages/ChatListPage.qml:462
msgid "Do you want to send the imported text to %1?"
msgstr "Vuoi inviare il testo importato a %1?"

#: ../app/qml/pages/ChatListPage.qml:464
#: ../app/qml/pages/MessageListPage.qml:854
msgid "Send"
msgstr "Invia"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:28
msgid "Connecting"
msgstr "Connessione in corso"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:26
msgid "Offline"
msgstr "Offline"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:30
#: ../libs/qtdlib/user/qtduserstatus.cpp:78
msgid "Online"
msgstr "in linea"

#: ../app/qml/pages/ConnectivityPage.qml:27
msgid "Connecting To Proxy"
msgstr "Connessione Al Proxy"

#: ../app/qml/pages/ConnectivityPage.qml:29
msgid "Updating"
msgstr "Aggiornando"

#: ../app/qml/pages/ConnectivityPage.qml:35
msgid "Connectivity"
msgstr "Connettività"

#: ../app/qml/pages/ConnectivityPage.qml:73
msgid "Telegram connectivity status:"
msgstr "Stato di connettività di Telegram:"

#: ../app/qml/pages/ConnectivityPage.qml:80
msgid "Ubuntu Touch connectivity status:"
msgstr "stato di connettività di Ubuntu Touch:"

#: ../app/qml/pages/ConnectivityPage.qml:87
msgid "Ubuntu Touch bandwith limited"
msgstr "Potenza collegamento a internet di Ubuntu Touch limitata"

#: ../app/qml/pages/ConnectivityPage.qml:87
msgid "Ubuntu Touch bandwith not limited"
msgstr "Potenza collegamento a internet di Ubuntu Touch senza limiti"

#: ../app/qml/pages/LogoutPage.qml:14
msgid "Good bye!"
msgstr "Arrivederci!"

#: ../app/qml/pages/LogoutPage.qml:27
msgid "Disconnecting..."
msgstr "Disconnessione..."

#: ../app/qml/pages/LogoutPage.qml:37
msgid ""
"The app will close automatically when the logout process ends.\n"
"Please, don't close it manually!"
msgstr ""
"L'app si chiuderà in automatico quando il processo di disconnessione "
"finirà.\n"
"Per favore, non chiudere l'app manualmente!"

#: ../app/qml/pages/MessageListPage.qml:35
msgid ", %1 online"
msgid_plural ", %1 online"
msgstr[0] ", %1 in linea"
msgstr[1] ", %1 in linea"

#: ../app/qml/pages/MessageListPage.qml:131
msgid "Telegram"
msgstr "Telegram"

#: ../app/qml/pages/MessageListPage.qml:337
msgid "You are not allowed to post in this channel"
msgstr "Non puoi postare in questo canale"

#: ../app/qml/pages/MessageListPage.qml:341
msgid "Waiting for other party to accept the secret chat..."
msgstr "In attesa che l'altro partecipante accetti la chat segreta..."

#: ../app/qml/pages/MessageListPage.qml:343
msgid "Secret chat has been closed"
msgstr "La chat segreta è stata chiusa"

#: ../app/qml/pages/MessageListPage.qml:351
msgid "You left this group"
msgstr "Hai lasciato il gruppo"

#: ../app/qml/pages/MessageListPage.qml:354
msgid "You have been banned"
msgstr "Sei stato bannato"

#: ../app/qml/pages/MessageListPage.qml:357
msgid "You are not allowed to post in this group"
msgstr "Non puoi postare in questo gruppo"

#: ../app/qml/pages/MessageListPage.qml:361
msgid "You can't write here. Reason unkown"
msgstr "Non puoi scrivere qui. Motivo sconosciuto"

#: ../app/qml/pages/MessageListPage.qml:384
msgid "Join"
msgstr "Unisciti"

#: ../app/qml/pages/MessageListPage.qml:524
msgid "Type a message..."
msgstr "Scrivi un messaggio..."

#: ../app/qml/pages/MessageListPage.qml:742
msgid "<<< Swipe to cancel"
msgstr "<<< Scorri per annullare"

#: ../app/qml/pages/MessageListPage.qml:852
msgid "Do you want to share your location with %1?"
msgstr "Vuoi condividere la tua posizione con %1?"

#: ../app/qml/pages/MessageListPage.qml:865
msgid "Requesting location from OS..."
msgstr "Richiesta posizione al SO..."

#: ../app/qml/pages/PickerPage.qml:16
msgid "Content Picker"
msgstr "Selezionatore di contenuti"

#: ../app/qml/pages/PreviewPage.qml:35
msgid "File: "
msgstr "File: "

#: ../app/qml/pages/SecretChatKeyHashPage.qml:66
msgid ""
"Check the image or the text. If they match with the ones on <b>%1</b>'s "
"device, end-to-end cryptography is granted."
msgstr ""
"Controlla l'immagine o il testo. Se coincidono con quelli sul dispositivo di "
"<b>%1</b>, la crittografia end-to-end è garantita."

#: ../app/qml/pages/SettingsPage.qml:69 ../app/qml/pages/SettingsPage.qml:141
msgid "Logout"
msgstr "Disconnetti"

#: ../app/qml/pages/SettingsPage.qml:85
msgid "Delete account"
msgstr "Elimina account"

#: ../app/qml/pages/SettingsPage.qml:101
msgid "Connectivity status"
msgstr "Stato di connessione"

#: ../app/qml/pages/SettingsPage.qml:118
msgid "Toggle message status indicators"
msgstr "Mostra lo stato del messaggio con un icona"

#: ../app/qml/pages/SettingsPage.qml:139
msgid ""
"Warning: Logging out will delete all local data from this device, including "
"secret chats. Are you still sure you want to log out?"
msgstr ""
"Attenzione: Disconnettendosi perderai tutti i dati di questo dispositivo, "
"chat segrete incluse. Sei ancora sicuro di volerti disconnettere?"

#: ../app/qml/pages/SettingsPage.qml:149
msgid ""
"Warning: Deleting the account will delete all the data you ever received or "
"sent using telegram except for data you have explicitly saved outside the "
"telegram cloud. Are you really really sure you want to delete your telegram "
"account?"
msgstr ""
"Attenzione: Eliminando l'account eliminerai tutti i dati che hai ricevuto o "
"inviato usando telegram tranne per i dati che hai salvato esplicitamente al "
"di fuori del cloud di telegram. Sei veramente sicuro che vuoi eliminare il "
"tuo account telegram?"

#: ../app/qml/pages/UserListPage.qml:38
msgid "Add Contact"
msgstr "Aggiungi Contatto"

#: ../app/qml/pages/UserListPage.qml:43
msgid "Import Contact"
msgstr "Importa contatto"

#: ../app/qml/pages/UserListPage.qml:55
msgid "The contact will be added. First and last name are optional"
msgstr "Il contatto verrà aggiunto. Nome e cognome sono opzionali"

#: ../app/qml/pages/UserListPage.qml:57
msgid "Add"
msgstr "Aggiungi"

#: ../app/qml/pages/UserListPage.qml:136
msgid "Secret Chat"
msgstr "Chat Segreta"

#: ../app/qml/pages/UserListPage.qml:213
msgid "The contact will be deleted. Are you sure?"
msgstr "Il contatto verrà eliminato. Sei sicuro?"

#: ../app/qml/pages/WaitCodePage.qml:17
msgid "Enter Code"
msgstr "Inserisci Codice"

#: ../app/qml/pages/WaitCodePage.qml:39
msgid "Code"
msgstr "Codice"

#: ../app/qml/pages/WaitCodePage.qml:54
msgid ""
"A code was sent via Telegram to your other devices. Please enter it here."
msgstr ""
"Un codice è stato mandato via Telegram a un altro tuo dispositivo. "
"Inseriscilo qui per favore."

#: ../app/qml/pages/WaitPasswordPage.qml:18
msgid "Enter Password"
msgstr "Inserisci Password"

#: ../app/qml/pages/WaitPasswordPage.qml:39
msgid "Password"
msgstr "Password"

#: ../app/qml/pages/WaitPasswordPage.qml:57
msgid "Password hint: %1"
msgstr "Suggerimento password: %1"

#: ../app/qml/pages/WaitPasswordPage.qml:62
#: ../app/qml/pages/WaitPhoneNumberPage.qml:92
#: ../app/qml/pages/WaitRegistrationPage.qml:57
msgid "Next..."
msgstr "Avanti..."

#: ../app/qml/pages/WaitPhoneNumberPage.qml:17
msgid "Enter Phone Number"
msgstr "Inserisci Numero di Telefono"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:54
#: ../app/qml/pages/WaitPhoneNumberPage.qml:64
msgid "Phone number"
msgstr "Numero di telefono"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:88
msgid "Please confirm your country code and enter your phone number."
msgstr ""
"Conferma il codice del tuo Paese e inserisci il tuo numero di telefono."

#: ../app/qml/pages/WaitRegistrationPage.qml:17
msgid "Enter your Name"
msgstr "Inserisci il tuo Nome"

#: ../app/qml/pages/WaitRegistrationPage.qml:38
msgid "First Name"
msgstr "Nome"

#: ../app/qml/pages/WaitRegistrationPage.qml:44
msgid "Last Name"
msgstr "Cognome"

#: ../app/qml/stores/AuthStateStore.qml:60
msgid "Invalid phone number!"
msgstr "Numero di telefono invalido!"

#: ../app/qml/stores/AuthStateStore.qml:67
msgid "Invalid code!"
msgstr "Codice invalido!"

#: ../app/qml/stores/AuthStateStore.qml:74
msgid "Invalid password!"
msgstr "Password invalida!"

#: ../app/qml/stores/AuthStateStore.qml:94
msgid "Auth code not expected right now"
msgstr "Codice di autenticazione non atteso adesso"

#: ../app/qml/stores/AuthStateStore.qml:100
msgid "Oops! Internal error."
msgstr "Oops! errore interno."

#: ../app/qml/stores/AuthStateStore.qml:119
msgid "Registration not expected right now"
msgstr "Codice di registrazione non atteso adesso"

#: ../app/qml/stores/ChatStateStore.qml:42
#: ../app/qml/stores/ChatStateStore.qml:48
msgid "Error"
msgstr "Errore"

#: ../app/qml/stores/ChatStateStore.qml:42
msgid "No valid location received after 180 seconds!"
msgstr "Nessuna posizione valida ricevuta dopo 180 secondi!"

#: ../app/qml/stores/ChatStateStore.qml:48
msgid "Username <b>@%1</b> not found"
msgstr "Utente <b>%1</b> non trovato"

#: ../app/qml/stores/NotificationsStateStore.qml:12
msgid "Push Registration Failed"
msgstr "Registrazione Push Fallita"

#: ../app/qml/stores/NotificationsStateStore.qml:23
msgid "No Ubuntu One"
msgstr "No Ubuntu One"

#: ../app/qml/stores/NotificationsStateStore.qml:24
msgid "Please connect to Ubuntu One to receive push notifications."
msgstr "Per favore connettiti a Ubuntu One per ricevere notifiche push."

#: ../libs/qtdlib/chat/qtdchat.cpp:326
msgid "Draft:"
msgstr "Bozza:"

#: ../libs/qtdlib/chat/qtdchat.cpp:622
msgid "is choosing contact..."
msgstr "sta scegliendo un contatto..."

#: ../libs/qtdlib/chat/qtdchat.cpp:623
msgid "are choosing contact..."
msgstr "stanno scegliendo contatti..."

#: ../libs/qtdlib/chat/qtdchat.cpp:626
msgid "is choosing location..."
msgstr "sta scegliendo la posizione..."

#: ../libs/qtdlib/chat/qtdchat.cpp:627
msgid "are choosing location..."
msgstr "stanno scegliendo la posizione..."

#: ../libs/qtdlib/chat/qtdchat.cpp:632
msgid "is recording..."
msgstr "sta registrando..."

#: ../libs/qtdlib/chat/qtdchat.cpp:633
msgid "are recording..."
msgstr "stanno registrando..."

#: ../libs/qtdlib/chat/qtdchat.cpp:636
msgid "is typing..."
msgstr "sta scrivendo..."

#: ../libs/qtdlib/chat/qtdchat.cpp:637
msgid "are typing..."
msgstr "stanno scrivendo..."

#: ../libs/qtdlib/chat/qtdchat.cpp:640
msgid "is doing something..."
msgstr "sta facendo qualcosa..."

#: ../libs/qtdlib/chat/qtdchat.cpp:641
msgid "are doing something..."
msgstr "stanno facendo qualcosa..."

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF"
msgstr "GIF"

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF,"
msgstr "GIF,"

#: ../libs/qtdlib/messages/content/qtdmessagebasicgroupchatcreate.cpp:25
#: ../libs/qtdlib/messages/content/qtdmessagesupergroupchatcreate.cpp:23
msgid "created this group"
msgstr "creato il gruppo"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:68
msgid "Call Declined"
msgstr "Chiamata Rifiutata"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:71
msgid "Call Disconnected"
msgstr "Chiamata Disconnessa"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:74
msgid "Call Ended"
msgstr "Chiamata Conclusa"

#. TRANSLATORS: This is a duration in hours:minutes:seconds format - only arrange the order, do not translate!
#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:78
msgid "%1:%2:%3"
msgstr "%1:%2:%3"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:79
msgid "Outgoing Call (%1)"
msgstr "Chiamata in uscita (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:80
msgid "Incoming Call (%1)"
msgstr "Chiamata in entrata (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Cancelled Call"
msgstr "Chiamata Annullata"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Missed Call"
msgstr "Chiamata Persa"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:86
msgid "Call"
msgstr "Chiamata"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "added one or more members"
msgstr "aggiunto uno o più membri"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "joined the group"
msgstr "entrato nel gruppo"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangephoto.cpp:20
msgid "changed the chat photo"
msgstr "cambiata la foto della chat"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangetitle.cpp:19
msgid "changed the chat title"
msgstr "cambiato il titolo della chat"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "left the group"
msgstr "ha lasciato il gruppo"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "removed a member"
msgstr "rimosso un membro"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletephoto.cpp:8
msgid "deleted the chat photo"
msgstr "eliminata la foto della chat"

#: ../libs/qtdlib/messages/content/qtdmessagechatjoinbylink.cpp:8
msgid "joined the group via the public link"
msgstr "entrato nel gruppo tramite link pubblico"

#: ../libs/qtdlib/messages/content/qtdmessagechatsetttl.cpp:19
msgid "message TTL has been changed"
msgstr "messaggio TTL é stato modificato"

#: ../libs/qtdlib/messages/content/qtdmessagechatupgradefrom.cpp:29
#: ../libs/qtdlib/messages/content/qtdmessagechatupgradeto.cpp:29
msgid "upgraded to supergroup"
msgstr "potenziato a supergruppo"

#: ../libs/qtdlib/messages/content/qtdmessagecontact.cpp:18
msgid "Contact"
msgstr "Contatto"

#: ../libs/qtdlib/messages/content/qtdmessagecontactregistered.cpp:9
msgid "has joined Telegram!"
msgstr "si è unito a Telegram!"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:19
msgid "Today"
msgstr "Oggi"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:21
msgid "Yesterday"
msgstr "Ieri"

#. TRANSLATORS: String in date separator label. For messages within a week: full weekday name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:24
msgid "dddd"
msgstr "dddd"

#. TRANSLATORS: String in date separator label. For messages of pas years: date number, month name and year
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:27
msgid "dd MMMM yyyy"
msgstr "dd MMMM yyyy"

#. TRANSLATORS: String in date separator label. For messages older that a week but within the current year: date number and month name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:30
msgid "dd MMMM"
msgstr "dd MMMM"

#: ../libs/qtdlib/messages/content/qtdmessagelocation.cpp:19
msgid "Location"
msgstr "Posizione"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo"
msgstr "Foto"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo,"
msgstr "Foto,"

#: ../libs/qtdlib/messages/content/qtdmessagepinmessage.cpp:19
msgid "Pinned Message"
msgstr "Messaggi fissati"

#: ../libs/qtdlib/messages/content/qtdmessagesticker.cpp:20
msgid "Sticker"
msgstr "Sticker"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video"
msgstr "Video"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video,"
msgstr "Video,"

#: ../libs/qtdlib/messages/content/qtdmessagevideonote.cpp:30
msgid "Video message"
msgstr "Video messaggio"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message"
msgstr "Messaggio vocale"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message,"
msgstr "Messaggio vocale,"

#: ../libs/qtdlib/messages/forwardinfo/qtdmessageforwardinfo.cpp:61
msgid "Unknown origin"
msgstr "Origine sconosciuta"

#: ../libs/qtdlib/messages/qtdmessage.cpp:85
msgid "Me"
msgstr "Io"

#: ../libs/qtdlib/messages/qtdmessagecontentfactory.cpp:133
msgid "Unimplemented:"
msgstr "Non implementato:"

#: ../libs/qtdlib/messages/qtdmessagelistmodel.cpp:250
msgid "Unread Messages"
msgstr "Messaggi non letti"

#: ../libs/qtdlib/user/qtduserstatus.cpp:28
msgid "Last seen one month ago"
msgstr "Ultimo accesso un mese fa"

#: ../libs/qtdlib/user/qtduserstatus.cpp:39
msgid "Last seen one week ago"
msgstr "Ultimo access una settimana fa"

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "Last seen "
msgstr "Ultimo accesso "

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "dd.MM.yy hh:mm"
msgstr "dd/MM/yy hh:mm"

#: ../libs/qtdlib/user/qtduserstatus.cpp:96
msgid "Seen recently"
msgstr "Ultimo accesso"

#: ../push/pushhelper.cpp:124
msgid "sent you a message"
msgstr "Ti ha inviato un messaggio"

#: ../push/pushhelper.cpp:128
msgid "sent you a photo"
msgstr "Ti ha inviato una foto"

#: ../push/pushhelper.cpp:132
msgid "sent you a sticker"
msgstr "Ti ha inviato uno sticker"

#: ../push/pushhelper.cpp:136
msgid "sent you a video"
msgstr "Ti ha inviato un video"

#: ../push/pushhelper.cpp:140
msgid "sent you a document"
msgstr "Ti ha inviato un documento"

#: ../push/pushhelper.cpp:144
msgid "sent you an audio message"
msgstr "Ti ha inviato un audio"

#: ../push/pushhelper.cpp:148
msgid "sent you a voice message"
msgstr "Ti ha inviato un messaggio vocale"

#: ../push/pushhelper.cpp:152
msgid "shared a contact with you"
msgstr "Ha condiviso un contatto con te"

#: ../push/pushhelper.cpp:156
msgid "sent you a map"
msgstr "Ti ha inviato una mappa"

#: ../push/pushhelper.cpp:161
msgid "%1: %2"
msgstr "%1: %2"

#: ../push/pushhelper.cpp:166
msgid "%1 sent a message to the group"
msgstr "%1 ha inviato un messaggio al gruppo"

#: ../push/pushhelper.cpp:171
msgid "%1 sent a photo to the group"
msgstr "%1 ha inviato una foto al gruppo"

#: ../push/pushhelper.cpp:176
msgid "%1 sent a sticker to the group"
msgstr "%1 ha inviato uno sticker al gruppo"

#: ../push/pushhelper.cpp:181
msgid "%1 sent a video to the group"
msgstr "%1 ha inviato un video al gruppo"

#: ../push/pushhelper.cpp:186
msgid "%1 sent a document to the group"
msgstr "%1 ha inviato un documento al gruppo"

#: ../push/pushhelper.cpp:191
msgid "%1 sent a voice message to the group"
msgstr "%1 ha inviato un messaggio vocale al gruppo"

#: ../push/pushhelper.cpp:196
msgid "%1 sent a GIF to the group"
msgstr "%1 ha inviato una GIF al gruppo"

#: ../push/pushhelper.cpp:201
msgid "%1 sent a contact to the group"
msgstr "%1 ha inviato un contatto al gruppo"

#: ../push/pushhelper.cpp:206
msgid "%1 sent a map to the group"
msgstr "%1 ha inviato una mappa al gruppo"

#: ../push/pushhelper.cpp:211 ../push/pushhelper.cpp:232
msgid "%1 invited you to the group"
msgstr "%1 ti ha invitato nel gruppo"

#: ../push/pushhelper.cpp:216
msgid "%1 changed group name"
msgstr "%1 ha cambiato il nome del gruppo"

#: ../push/pushhelper.cpp:221
msgid "%1 changed group photo"
msgstr "%1 ha cambiato la foto del gruppo"

#. TRANSLATORS: Notification message saying: person A invited person B (to a group)
#: ../push/pushhelper.cpp:227
msgid "%1 invited %2"
msgstr "%1 ha invitato %2"

#: ../push/pushhelper.cpp:243
msgid "%1 removed you from the group"
msgstr "%1 ti ha rimosso dal gruppo"

#: ../push/pushhelper.cpp:248
msgid "%1 has left the group"
msgstr "%1 ha lasciato il gruppo"

#: ../push/pushhelper.cpp:253
msgid "%1 has returned to the group"
msgstr "%1 ha fatto ritorno nel gruppo"

#. TRANSLATORS: This format string tells location, like: @ McDonals, New York
#: ../push/pushhelper.cpp:258
msgid "@ %1"
msgstr "@ %1"

#. TRANSLATORS: This format string tells who has checked in (in a geographical location).
#: ../push/pushhelper.cpp:260
msgid "%1 has checked-in"
msgstr "%1 ha controllato"

#. TRANSLATORS: This format string tells who has just joined Telegram.
#: ../push/pushhelper.cpp:266
msgid "%1 joined Telegram!"
msgstr "%1 é entrato in Telegram!"

#: ../push/pushhelper.cpp:271 ../push/pushhelper.cpp:277
msgid "New login from unrecognized device"
msgstr "Nuovo accesso da dispositivo non riconosciuto"

#. TRANSLATORS: This format string indicates new login of: (device name) at (location).
#: ../push/pushhelper.cpp:276
msgid "%1 @ %2"
msgstr "%1 @ %2"

#: ../push/pushhelper.cpp:281
msgid "updated profile photo"
msgstr "caricata foto profilo"

#: ../push/pushhelper.cpp:286 ../push/pushhelper.cpp:291
#: ../push/pushhelper.cpp:296
msgid "You have a new message"
msgstr "Hai un nuovo messaggio"

#~ msgid "Animated stickers not supported yet :("
#~ msgstr "Adesivi animati non ancora supportati :("

#~ msgid "not available"
#~ msgstr "non disponibile"

#~ msgid "MMMM dd, yyyy"
#~ msgstr "MMMM gg, aaaa"

#~ msgid "MMMM dd"
#~ msgstr "MMMM gg"

#~ msgid "Incorrect auth code length."
#~ msgstr "Lunghezza del codice di autenticazione errata."

#~ msgid "Phone call"
#~ msgstr "Chiamata"

#~ msgid "sent an audio message"
#~ msgstr "mandato un audio"

#~ msgid "sent a photo"
#~ msgstr "mandata una foto"

#~ msgid "sent a video"
#~ msgstr "mandato un video"

#~ msgid "sent a video note"
#~ msgstr "mandata una nota video"

#~ msgid "sent a voice note"
#~ msgstr "mandata una nota audio"

#~ msgid "joined by invite link"
#~ msgstr "unito tramite link invito"

#~ msgid "Image"
#~ msgstr "Immagine"

#~ msgid "File"
#~ msgstr "File"

#~ msgid "sent an unknown message: %1"
#~ msgstr "inviato un messaggio sconosciuto: %1"

#, fuzzy
#~| msgid "dd MMMM"
#~ msgid "dd MM"
#~ msgstr "dd MMMM"

#~ msgid "From: "
#~ msgstr "Da:"

#~ msgid "+"
#~ msgstr "+"
